class FocusKeeper {
  constructor() {
    this.handleBlur = this.handleBlur.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.keepFocus = this.keepFocus.bind(this);
    this.screenHeight = window.innerHeight;
    window.addEventListener("resize", () => {
      if(this.screenHeight === window.innerHeight) {
        //Keyboard was closed, lose focus.
        this.target.blur();
        this.wasFocused = false;
      }
    });
  }
  setTarget(target){
    this.target = target;
  }
  handleBlur(e){
    this.focusedInputHeight = this.target.clientHeight;
    this.wasFocused = true;
  }
  handleFocus(e){
    this.focusedInputHeight = this.target.clientHeight;
  }
  keepFocus(){
    if (this.target.clientHeight === this.focusedInputHeight && this.wasFocused)
      this.target.focus();
    this.wasFocused = false;
  }
}

export default FocusKeeper;
