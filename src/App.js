import React, { Component } from 'react';
import FocusKeeper from './FocusKeeper';
import localforage from 'localforage';
import './App.css';
const maxLen = 400;

class App extends Component {
  constructor() {
    super();
    this.state = {
      orientation: "landscape",
    }
    this.updateOrientation = this.updateOrientation.bind(this);
    this.goToHistory = this.goToHistory.bind(this);
    this.exitHistory = this.exitHistory.bind(this);
    localforage.length((err, len) => {
      console.log("length of offline store: " + len);
    });
  }

  goToHistory() {
    this.app.classList.add("scrolled");
  }

  exitHistory() {
    this.app.classList.remove("scrolled");
  }

  updateOrientation() {
    this.setState({
      orientation: (window.screen.width > window.screen.height)? "landscape":"portrait",
    });
    this.forceUpdate();
  }

  componentDidMount() {
    this.updateOrientation();
    window.addEventListener("resize", this.updateOrientation);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateOrientation);
  }

  render() {
    return (
      <div>
      <div
      ref={el => this.app = el}
      className={"App-" + this.state.orientation}>
        <Main
          orientation={this.state.orientation}
          historyCallback={this.goToHistory}/>
        <History
          orientation={this.state.orientation}
          exitCallback={this.exitHistory}/>
      </div>
      <LandingPage/>
      </div>
    );
  }
}

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      draftLen: 0,
    }
    this.focusKeeper = new FocusKeeper();
    this.toggleInspiration = this.toggleInspiration.bind(this);
    this.goToHistory = this.props.historyCallback;
    this.onDraftChange = this.onDraftChange.bind(this);

    //Retrieve stored draft
    this.draft = "";
    localforage.getItem('draft', (err, value) => {
      if(value){
        console.log("Draft retrieved: " + value);
        this.draft = value;
        if(value && this.saveButton && this.saveButton.classList.contains("inactive")){
          this.saveButton.classList.remove("inactive");
        }
        if(this.textArea) {
          this.textArea.value = this.draft;
          this.setState({
            draftLen: this.draft.length,
          });
        }
        this.forceUpdate();
      }
    });
  }

  onDraftChange(event) {
    if(event.target.value || event.target.value === ""){
      const draftLen = event.target.value.length;
      if(draftLen && this.saveButton.classList.contains("inactive")){
        this.saveButton.classList.remove("inactive");
      } else if (!draftLen && !(this.saveButton.classList.contains("inactive"))){
        this.saveButton.classList.add("inactive");
      }
      this.setState({
        draftLen: draftLen,
      });

      //Update stored draft
      localforage.setItem('draft', event.target.value, (err, val) =>{
        this.draft = val;
        console.log("Draft saved: " + val);
      });
    }
  }

  toggleInspiration() {
    this.focusKeeper.keepFocus();
    if(this.inspirationDiv.classList.contains("closed")) {
      this.inspirationButton.classList.add("active");
      this.inspirationDiv.classList.remove("closed");
    } else {
      this.inspirationButton.classList.remove("active");
      this.inspirationDiv.classList.add("closed");
    }
  }

  render() {
    const isLandscape = this.props.orientation==="landscape";
    return (
      <div className={"App-main-" + this.props.orientation}>
        <div className="App-main-intro">
          <p>What are you grateful for today?</p>
          <i
            ref={el => this.inspirationButton = el}
            className="material-icons md-24 button"
            onClickCapture={this.toggleInspiration}>
            lightbulb_outline
          </i>
          <i className={"material-icons md-24 button" + (isLandscape?" hidden":"")} onClick={this.goToHistory}>history</i>
        </div>
        <div
          ref={el => this.inspirationDiv = el}
          className="App-main-inspiration closed">
          <p>Who helped you today?</p>
          <i className="material-icons md-18">info_outline</i>
        </div>
        <textarea
          ref={el => {this.textArea = el; this.focusKeeper.setTarget(el);}}
          className="App-main-text-entry"
          placeholder="Today I'm grateful for..."
          onInput={this.onDraftChange}
          onBlur={this.focusKeeper.handleBlur}
          onFocus={this.focusKeeper.handleFocus}
          maxLength={maxLen}>
        </textarea>
        <div className="App-main-footer">
          <p>{this.state.draftLen} / {maxLen} characters</p>
          <i
            ref={el => this.saveButton = el}
            className={"material-icons md-24 button" + (this.draft ? "":" inactive")}>done</i>
        </div>
      </div>
    );
  }
}

class History extends Component {
  constructor(props) {
    super(props);
    this.exitHistory = this.props.exitCallback;
  }
  render() {
    const isLandscape = this.props.orientation==="landscape";
    return (
        <div className={"App-history-" + this.props.orientation}>
          <div className="App-main-intro">
            <p><b>History</b></p>
            <i className="material-icons md-24 button">search</i>
            <i className={"material-icons md-36 button" + (isLandscape?" hidden":"")}
              onClick={this.exitHistory}>keyboard_arrow_up</i>
          </div>
          <div className="history-list">
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi nunc, ultricies convallis tincidunt ut, bibendum eget neque. Nunc sollicitudin et justo quis aliquam. Mauris eu vulputate enim. In sit amet massa in ligula feugiat laoreet. Aliquam efficitur nunc urna, eget mattis dui sodales ac. Duis eu tincidunt felis. Cras ac lectus vel felis porta consectetur sed vel tortor. Fusce eget finibus sem, non pulvinar mi. Ut quis posuere velit.

Nunc tortor felis, sollicitudin non orci sed, luctus convallis urna. Vivamus semper interdum felis. Aenean id ultrices odio, et sollicitudin massa. Sed quam erat, volutpat quis faucibus consectetur, ullamcorper nec purus. Nam maximus condimentum eros, eu consequat nisl rutrum faucibus. Proin rutrum quam nisl, non rutrum purus tempus sed. Mauris vitae tempor nisl, vitae fringilla neque. Proin eget lectus sed ex vulputate mattis eu a velit. In maximus dolor ut tincidunt lobortis. Etiam lorem nunc, vehicula eget augue nec, aliquam aliquet leo. Nunc vel tincidunt enim.

Nulla ac ipsum dictum, tincidunt arcu sit amet, porttitor turpis. Sed commodo tristique mi, id egestas ipsum consequat lacinia. Fusce quis odio orci. Cras fermentum fermentum dui eget convallis. Ut vitae pharetra magna. Etiam enim ante, convallis vel pulvinar in, porttitor in tortor. Praesent mollis ultricies diam, vel vehicula massa scelerisque nec. Aenean lacinia sit amet elit eu venenatis. Morbi ante mauris, euismod ac enim id, convallis tempor nunc.

Nullam et eros vitae nunc interdum fermentum eget quis nulla. Aenean at volutpat tortor. Curabitur et consequat lorem, eget vehicula orci. Praesent bibendum ligula vel tellus viverra, in dignissim turpis vulputate. Proin hendrerit gravida lorem, non placerat nisl congue tempus. Sed sed magna tincidunt, vulputate justo eu, suscipit tortor. Nam et maximus nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vulputate luctus placerat. Donec id turpis eu nisi mattis cursus quis sed enim. Donec scelerisque orci vitae pellentesque faucibus. Sed at tincidunt diam, ut molestie velit. Nulla efficitur magna in est pellentesque, et eleifend diam vestibulum. Praesent tempus odio in fringilla venenatis. Ut pulvinar, lectus quis laoreet tristique, justo elit porta erat, at placerat nulla nisl in velit. Suspendisse pulvinar elit quis massa convallis, id dignissim mauris dignissim.

Donec at euismod sem. Quisque venenatis interdum aliquet. Quisque vel velit magna. Vestibulum non lorem ex. Aenean a vehicula purus. Donec tellus est, vehicula lacinia eleifend in, auctor vitae nisl. Nullam ultricies ultricies elementum. Ut pellentesque tortor quis velit fermentum, quis hendrerit ligula ornare. Nunc at fermentum diam, eu semper orci. Nullam laoreet dolor quis ullamcorper sagittis. Cras feugiat ante metus, sed congue mauris posuere sed.
            </p>
          </div>
          <div className="App-main-footer">
            <i
              ref={el => this.prevButton = el}
              className={"material-icons md-24 button"}>keyboard_arrow_left</i>
            <p> Sep 2017 </p>
            <i
              ref={el => this.nextButton = el}
              className={"material-icons md-24 button"}>keyboard_arrow_right</i>
          </div>
        </div>
    );
  }
}

class LandingPage extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    localforage.length((err, len) => {
      if(len){
        console.log("Returning user detected in constructor");
        this.returningUser = true;
        document.getElementById("landing-content").classList.add("clicked");
        document.getElementsByClassName("App-bar")[0].classList.add("down");
        this.forceUpdate();
      }
    });
  }

  onClick(e) {
    e.target.classList.add("clicked");
    document.getElementById("landing-content").classList.add("clicked");
    document.getElementsByClassName("App-bar")[0].classList.add("down");
  }

  render() {
    return (
      <div id="landing-content" className={this.returningUser ? " clicked":""}>
        <div className={"App-bar" + (this.returningUser ? " down" : "")}>
          <h2>
            Gratitude App
          </h2>
        </div>
        <div className="content">
          <h1>
            Gratitude App
          </h1>
          <p>
            This is a web app where you can record what you're grateful for each day. Everything is stored on your device, so it's completely secure!
          </p>
          <button
          id="start-button"
          onClick={this.onClick}>
          Get Started
          </button>
        </div>
      </div>
    );
  }
}

export default App;
